mod hid;

use anyhow::{Context, Result};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Arguments {
    #[structopt(parse(from_os_str), short, long, default_value = "/dev/hidg0")]
    device_path: PathBuf,
    #[structopt(short, long, default_value = "10")]
    wait_speed_ms : u64,
}

struct Core {
    arguments : Arguments,
    device : std::fs::File,
}

impl Core {
    fn new() -> Result<Core> {
        let a = Arguments::from_args();
        let f = std::fs::OpenOptions::new().append(true).open(&a.device_path)
            .with_context(|| format!("Unable to open the file {:?}", &a.device_path))?;
        Ok(Core{arguments : a, device : f})
    }

    fn send_and_wait(&mut self, key : u8) -> Result<()> {
        const KEY_INFO_NULL : [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];

        // construct key info
        let mut key_info : [u8; 8] = KEY_INFO_NULL.clone();
        key_info[3] = key;

        // write to device
        use std::io::Write;
        self.device.write_all(&key_info)?;

        // pause
        std::thread::sleep(std::time::Duration::from_millis(self.arguments.wait_speed_ms));

        // write to device
        self.device.write_all(&KEY_INFO_NULL)?;

        Ok(())
    }

    fn execute(&mut self) -> Result<()> {
        // first, type "Success"
        const STRING : &str = "Success";
        for c in STRING.chars() {
            self.send_and_wait(hid::get_keyboard_usage_id(&c.to_string())?)?;
        }

        // loop num_lock
        let key_code = hid::get_keyboard_usage_id(&"numlock".to_string())?;
        loop {
            self.send_and_wait(key_code)?;
            std::thread::sleep(std::time::Duration::from_millis(100));
            self.send_and_wait(key_code)?;
            std::thread::sleep(std::time::Duration::from_secs(60));
        }
    }
}

fn main() -> Result<()> {
    let mut core = Core::new()?;
    core.execute()
}
